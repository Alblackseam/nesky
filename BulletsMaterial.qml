import Qt3D.Core 2.0
import Qt3D.Render 2.0


Material {
	id:root
	property color ambient:  Qt.rgba( 0.05, 0.05, 0.05, 1.0 )
	property color diffuse:  Qt.rgba( 0.7, 0.7, 0.7, 1.0 )
	property color specular: Qt.rgba( 0.95, 0.95, 0.95, 1.0 )
	property real shininess: 150.0


	effect: Effect {
		parameters: [
			Parameter { name: "ka";   value: Qt.vector3d(root.ambient.r, root.ambient.g, root.ambient.b) },
			Parameter { name: "kd";   value: Qt.vector3d(root.diffuse.r, root.diffuse.g, root.diffuse.b) },
			Parameter { name: "ks";  value: Qt.vector3d(root.specular.r, root.specular.g, root.specular.b) },
			Parameter { name: "shininess"; value: root.shininess },
			Parameter { name: "lightPosition"; value: Qt.vector4d(1.0, 1.0, 0.0, 1.0) },
			Parameter { name: "lightIntensity"; value: Qt.vector3d(1.0, 1.0, 1.0) }
		]

		RenderPass {
			id: glpass
			filterKeys: [ FilterKey { name: "pass"; value: "forward" } ]

			shaderProgram: ShaderProgram {
				vertexShaderCode: loadSource("qrc:/Shaders/Bullets.vert")
				fragmentShaderCode: loadSource("qrc:/Shaders/Bullets.frag")
			}
		}

		techniques: [
			Technique {
				graphicsApiFilter {
					api: GraphicsApiFilter.OpenGL
					profile: GraphicsApiFilter.CoreProfile
					majorVersion: 3
					minorVersion: 2
				}

				filterKeys: [ FilterKey { name: "renderingStyle"; value: "forward" } ]
				renderPasses: [ glpass ]
			}
		]
	}
}

