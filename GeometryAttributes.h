#pragma once

#include <deque>
#include <tuple>
#include <utility>

#include <QObject>
#include <QVector3D>
#include <QColor>
#include <QMatrix4x4>
#include <Qt3DRender/qgeometryrenderer.h>
#include <Qt3DRender/qattribute.h>
#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qgeometry.h>
#include <QCylinderGeometry>
#include "QPlace.h"

template<std::size_t I = 0, typename FuncT, typename... Tp>
static inline typename std::enable_if_t<I == sizeof...(Tp), void>
for_each(const std::tuple<Tp...> &, FuncT) // Unused arguments are given no names.
{
}

template<std::size_t I = 0, typename FuncT, typename... Tp>
static inline typename std::enable_if_t<I < sizeof...(Tp), void>
for_each(const std::tuple<Tp...>& t, FuncT f)
{
	f(std::get<I>(t));
	for_each<I + 1, FuncT, Tp...>(t, f);
}




template<Qt3DRender::QAttribute::VertexBaseType>
struct VertexElementTypeTraits{
};

/*
Byte = 0,
UnsignedByte,
Short,
UnsignedShort,
Int,
UnsignedInt,
HalfFloat,
Float,
Double
*/
template<>
struct VertexElementTypeTraits<Qt3DRender::QAttribute::Byte> {
	using Type = char;
};

template<>
struct VertexElementTypeTraits<Qt3DRender::QAttribute::UnsignedByte> {
	using Type = unsigned char;
};

template<>
struct VertexElementTypeTraits<Qt3DRender::QAttribute::Float> {
	using Type = float;
};


template<typename ElementType, Qt3DRender::QAttribute::VertexBaseType VertexElementType, size_t VertexElementCount, size_t Divisor = 0>
struct Attribute {
	static constexpr auto _VertexElementType = VertexElementType;
	static constexpr size_t _Divisor = Divisor;
	static constexpr size_t _VertexElementCount = VertexElementCount;
	static constexpr size_t _VertexElementSize = sizeof(typename VertexElementTypeTraits<_VertexElementType>::Type);

	static constexpr size_t attributeSize() {
		return _VertexElementSize * _VertexElementCount;
	}

	static std::string buffer(const ElementType& value){
		assert(attributeSize() == sizeof(value));
		std::string result(attributeSize(), char('\0'));
		memcpy(result.data(), &value, sizeof(value));
		return result;
	}

	QString name = Qt3DRender::QAttribute::defaultPositionAttributeName();
};


template<typename TupleAttributes, size_t I = 0>
static constexpr auto attributesStrideSize() {
	using AttributeType = typename std::tuple_element<I, TupleAttributes>::type;
	constexpr auto attributeSize = AttributeType::attributeSize();
	if constexpr(I + 1 < std::tuple_size<TupleAttributes>::value)
		return attributeSize + attributesStrideSize<TupleAttributes, I + 1>();
	else
		return attributeSize;
}



/*
template<typename ... ElementType>
struct BufferItem: public std::tuple<ElementType...>
{
	BufferItem(ElementType... args) : std::tuple<ElementType...>{ args...} {
	}
};

template<typename Attribute, typename ... Attributes>
static constexpr size_t attributesStrideSize(Attribute&, Attributes& ... attributes) {
	constexpr size_t attributeSize = Attribute::attributeSize();

	if constexpr(sizeof...(Attributes) > 0)
		return attributesStrideSize(attributes...) + attributeSize;
	else
		return attributeSize;
}


template<size_t Offset, typename Attribute, typename ... Attributes>
static constexpr auto attributesOffsets(Attribute&, Attributes& ... attributes) {
	constexpr size_t attributeSize = Attribute::attributeSize();

	if constexpr(sizeof...(Attributes) > 0){

		constexpr auto offsets = attributesOffsets<Offset + attributeSize>(attributes...);

		std::array<size_t, offsets.size() + 1> result{};
		result[0] = Offset;

		for (size_t i = 0; i < offsets.size(); i++)
			result[i+1] = offsets[i];
		return result;
	} else {
		return std::array<size_t, 1>{Offset};
	}
}
*/



static inline QVector3D toQVector3D(const QColor& color){
	return QVector3D{ static_cast<float>(color.redF()), static_cast<float>(color.greenF()), static_cast<float>(color.blueF()) };
}


template<typename TupleAttributes>
static constexpr auto convertToQAttributes(const TupleAttributes& attributes, Qt3DCore::QNode* parentNode = nullptr) {
	size_t strideSize = attributesStrideSize<TupleAttributes>(); // 36
	size_t offset = 0;
	std::vector<Qt3DRender::QAttribute*> result;

	for_each(attributes, [&result, &offset, strideSize, parentNode](auto& attribute){
		auto vertexBaseType = attribute._VertexElementType;
		auto vertexElementCount = attribute._VertexElementCount;
		auto attributeSize = attribute.attributeSize();

		auto qname = attribute.name;
		auto* att = new Qt3DRender::QAttribute(nullptr, qname, vertexBaseType, vertexElementCount, 0, offset, strideSize, parentNode);
		att->setDivisor(attribute._Divisor);
		result.push_back(att);

		offset += attributeSize;
	});

	return result;
}


template<typename Vertices>
//static inline QByteArray vertexDataToQByteArray(const std::vector<Vertex>& vertexData){
static inline QByteArray vertexDataToQByteArray(const Vertices& vertexData){
	using Vertex = typename Vertices::value_type;

	if (vertexData.size() == 0)
		return QByteArray{};

	QByteArray bufferData(vertexData.size() * Vertex::BufferSize, Qt::Uninitialized);

	for (size_t i = 0; i < vertexData.size(); ++i){
		auto buffer = vertexData[i].toBuffer();
		assert(buffer.size() == Vertex::BufferSize);
		memcpy(bufferData.data() + Vertex::BufferSize * i, buffer.data(), Vertex::BufferSize);
	}
	return bufferData;
}


