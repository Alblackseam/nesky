#pragma once

#include <deque>
#include <tuple>
#include <utility>

#include <QCylinderGeometry>
#include "Qt3DRender/QAttribute"
#include "GeometryAttributes.h"





struct VertexQVector3D {
	using PositionAttribute = Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 0>;
	using Attributes = std::tuple<
	    PositionAttribute
	>;
	static constexpr auto BufferSize = attributesStrideSize<Attributes>();

	QVector3D position;

	auto toBuffer() const {
		return PositionAttribute::buffer(position);
	}

	static inline auto getAttibutes(){
		return std::make_tuple(PositionAttribute{QString::fromLocal8Bit("vertexPosition")});
	}
};


struct Vertex {
	QMatrix4x4 place;
	//QVector3D position;
	//QVector3D direction;
	//QColor color;

	using PlaceAttribute = Attribute<QGenericMatrix<4,4,float>, Qt3DRender::QAttribute::Float, 16, 1>;
	//using DirectionAttribute = Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>;
	//using ColorAttribute = Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>;
	//using BoundingVolumePositionAttribute = Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>;
	using Attributes = std::tuple<
	    PlaceAttribute
	    //Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>,
	    //Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>
	>;
	static constexpr auto BufferSize = attributesStrideSize<Attributes>();

	auto toBuffer() const {
		QGenericMatrix<4,4,float> m44 = place.toGenericMatrix<4,4>();
		return PlaceAttribute::buffer(m44);
		//std::string positionBuffer = PositionAttribute::buffer(position);
		//std::string directionBuffer = DirectionAttribute::buffer(direction);
		//std::string colorBuffer = ColorAttribute::buffer(toQVector3D(color));
		//return positionBuffer + directionBuffer + colorBuffer;
	}



	static inline auto getAttibutes(){
		return std::make_tuple(
		    PlaceAttribute{QString::fromLocal8Bit("bulletPlace")}
		    //Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>{QString::fromLocal8Bit("bulletPosition")},
		    //Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>{QString::fromLocal8Bit("direction")},
		    //Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3, 1>{QString::fromLocal8Bit("color")}
		);
	}

};








class BulletsGeometry : public Qt3DExtras::QCylinderGeometry
{
	Q_OBJECT
	Q_PROPERTY(int bulletsCount MEMBER bulletsCount NOTIFY bulletsCountChanged)


public:
	BulletsGeometry(QNode *parent = nullptr)
		: Qt3DExtras::QCylinderGeometry{parent}
	{
		_buffer = new Qt3DRender::QBuffer();
		_buffer->setType(Qt3DRender::QBuffer::VertexBuffer);

		_boundingBuffer = new Qt3DRender::QBuffer();
		_boundingBuffer->setType(Qt3DRender::QBuffer::VertexBuffer);



		_attributes = convertToQAttributes(Vertex::getAttibutes(), parent);
		qDebug() << attributes().size();
		for (auto& attribute: _attributes)
			addAttribute(attribute);
		qDebug() << attributes().size();
	}



	void updateBuffer(const std::vector<Vertex>& vertexData){
		_bufferData = vertexDataToQByteArray(vertexData);

		_buffer->setData(_bufferData);

		for (auto& attribute: _attributes){
			attribute->setBuffer(_buffer);
			attribute->setCount(vertexData.size());
		}

		bulletsCount = vertexData.size();
		emit bulletsCountChanged(bulletsCount);
	}

public slots:
	void createBoundingBox(){
		auto boundingAttributes_Qt = convertToQAttributes(VertexQVector3D::getAttibutes(), nullptr);
		assert(boundingAttributes_Qt.size() == 1);
		std::vector<VertexQVector3D> boundingVertices;
		boundingVertices.push_back(VertexQVector3D{QVector3D{-1000,-1000,-1000}});
		boundingVertices.push_back(VertexQVector3D{QVector3D{-1000,-1000, 1000}});
		boundingVertices.push_back(VertexQVector3D{QVector3D{-1000, 1000,-1000}});
		boundingVertices.push_back(VertexQVector3D{QVector3D{-1000, 1000, 1000}});
		boundingVertices.push_back(VertexQVector3D{QVector3D{ 1000,-1000,-1000}});
		boundingVertices.push_back(VertexQVector3D{QVector3D{ 1000,-1000, 1000}});
		boundingVertices.push_back(VertexQVector3D{QVector3D{ 1000, 1000,-1000}});
		boundingVertices.push_back(VertexQVector3D{QVector3D{ 1000, 1000, 1000}});
		auto boundingBufferData = vertexDataToQByteArray(boundingVertices);
		_boundingBuffer->setData(boundingBufferData);

		Qt3DRender::QAttribute* boundingAttribute_Qt = boundingAttributes_Qt.front();
		assert(boundingAttribute_Qt);

		boundingAttribute_Qt->setBuffer(_boundingBuffer);
		boundingAttribute_Qt->setCount(boundingVertices.size());
		setBoundingVolumePositionAttribute(boundingAttribute_Qt);
	}

signals:
	void bulletsCountChanged(int);

private:
	int bulletsCount = 0;
	std::vector<Vertex> _vertices;
	QByteArray _bufferData;
	Qt3DRender::QBuffer* _buffer = nullptr;
	Qt3DRender::QBuffer* _boundingBuffer = nullptr;
	std::vector<Qt3DRender::QAttribute*> _attributes;
};

