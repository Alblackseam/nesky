import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Logic 2.0
import QtQml 2.2

Entity {
    id: root
    property Camera camera
	readonly property vector3d firstPersonUp: Qt.vector3d(0, 1, 0)
	property double distanceToTarget
	property int mousePosX
	property int mousePosY
	property int mouseLButtonDeltaX: 0
	property int mouseLButtonDeltaY: 0
	property int mouseRButtonDeltaX: 0
	property int mouseRButtonDeltaY: 0
	property int mouseMButtonDeltaX: 0
	property int mouseMButtonDeltaY: 0
	readonly property real rotate90Pixels: 520.0
	property vector3d referencePoint
	property bool referenceMode: false
	property vector3d translateWorldDelta
	property vector3d target: targetVector()
    property var keys: new Object
	property var helms: []


	onReferencePointChanged: {
		if (referenceMode===false) return;
		if (referencePoint===undefined) return;
		translateWorldDelta = referencePoint.minus(targetVector())
	}


	Component.onCompleted: {
		distanceToTarget = camera.viewCenter.minus(camera.position).length()
		console.log("distanceToTarget:", distanceToTarget)
	}

	function targetVector() {
		return camera.position.plus( camera.viewVector.normalized().times(distanceToTarget) );
	}

	function fractionToMeters(fraction){
		return fraction * 2.0 * distanceToTarget * Math.tan(camera.fieldOfView*0.017453292519943295);
		//console.log("fow:", camera.fieldOfView, " metersX:", metersX)
	}


	KeyboardDevice {
		id: keyboardDevice
	}

	KeyboardHandler{
		id: input
		sourceDevice: keyboardDevice
		focus: true

		onPressed: {
			keys[event.key] = true
		}

		onReleased: { //console.log("onReleased")
			keys[event.key] = false;
		}
	}



    MouseDevice {
        id: mouseSourceDevice
		//sensitivity: fineMotion.active ? 0.001 : 0.01
		//AxisSetting{ deadZoneRadius: 500.0 }
    }


	MouseHandler{
		sourceDevice: mouseSourceDevice
		onWheel: {
			var z;
			if (wheel.modifiers & Qt.AltModifier)
				z = (wheel.angleDelta.x / 120);
			else
				z = (wheel.angleDelta.y / 120);
			//console.log("wheel:",wheel.angleDelta.y," ", wheel.y )

			var new_distance = distanceToTarget*Math.pow(0.8,z);
			camera.setPosition( targetVector().plus(camera.viewVector.normalized().times(-new_distance)) );
			distanceToTarget = new_distance;
			wheel.accepted = true;
		}

		onPressed: {
            //console.log("onPressAndHold", mouse.x, mouse.y)
            mousePosX = mouse.x
            mousePosY = mouse.y

            if (mouse.buttons&Qt.RightButton){
                referenceMode = true
                //if (referencePoint===undefined) return
                //camera.viewCenter = referencePoint
                //var right = camera.viewVector.crossProduct(Qt.vector3d(0,1,0))
                //camera.upVector = right.crossProduct(camera.viewVector).normalized()
                //distanceToTarget = camera.viewCenter.minus(camera.position).length()
                //console.log("referenceMode on. distanceToTarget= ", distanceToTarget)
            }
        }

		onPressAndHold: {
		}

		onPositionChanged: {
			//console.log("onPositionChanged", mouse.x, mouse.button)

			if (mouse.buttons&Qt.LeftButton){
				mouseLButtonDeltaX += mouse.x - mousePosX
				mouseLButtonDeltaY += mouse.y - mousePosY
			}

			if (mouse.buttons&Qt.RightButton){
				mouseRButtonDeltaX += mouse.x - mousePosX
				mouseRButtonDeltaY += mouse.y - mousePosY
			}

			if (mouse.buttons&Qt.MiddleButton){
				mouseMButtonDeltaX += mouse.x - mousePosX
				mouseMButtonDeltaY += mouse.y - mousePosY
			}

			mousePosX = mouse.x
			mousePosY = mouse.y
		}

		onReleased: {
			mousePosX = mouse.x
			mousePosY = mouse.y
			referenceMode = mouse.buttons&Qt.RightButton;
		}
	}



    components: [
        FrameAction {
            onTriggered: {
				//console.log("width, height", width, ",", height, " camera:", camera.fieldOfView, " ", camera.aspectRatio, " ", camera.nearPlane );

				//console.log( keys[Qt.Key_Left] )
				for (var i=0; i<helms.length; i++){
					if (helms[i].handleKeys !== undefined)
						helms[i].handleKeys(keys)
					helms[i].update(dt, keys)
					helms[i].updateCamera(dt, camera)
				}

				var translateX = 0.0
				var translateY = 0.0
				var translateZ = 0.0
				var rotateX = 0.0
				var rotateY = 0.0
				var rotateZ = 0.0


				if ((mouseLButtonDeltaX!=0)||(mouseLButtonDeltaY!=0)){
					rotateX = -90.0 * mouseLButtonDeltaX/rotate90Pixels
					rotateY =  90.0 * mouseLButtonDeltaY/rotate90Pixels
				}

				if ((mouseRButtonDeltaX!=0)||(mouseRButtonDeltaY!=0)){
					rotateX = -90.0 * mouseRButtonDeltaX/rotate90Pixels
					rotateY =  90.0 * mouseRButtonDeltaY/rotate90Pixels
				}

				if ((mouseMButtonDeltaX!=0)||(mouseMButtonDeltaY!=0)){
					translateX += -mouseMButtonDeltaX/(width) * 2.0 * distanceToTarget * Math.tan(camera.fieldOfView*0.017453292519943295/2.0)*camera.aspectRatio
					translateY +=  mouseMButtonDeltaY/(height) * 2.0 * distanceToTarget * Math.tan(camera.fieldOfView*0.017453292519943295/2.0)
				}

				mouseLButtonDeltaX = mouseLButtonDeltaY = mouseRButtonDeltaX = mouseRButtonDeltaY = mouseMButtonDeltaX = mouseMButtonDeltaY = 0;

				//camera.translate(Qt.vector3d(tx.value, ty.value, tz.value).times(linearSpeed).times(dt))
				//var forward = camera.viewVector.normalized(); // console.log("forward:", forward)
				//var right = forward.crossProduct(camera.upVector.normalized()); // console.log("right:", right)
				//forward.y = 0;
				//right.y = 0;
				//camera.translateWorld( (forward.times(translateZ).plus(right.times(translateX))) )

				camera.setViewCenter( targetVector() )
				camera.tiltAboutViewCenter(rotateY )
				camera.panAboutViewCenter( rotateX, firstPersonUp )

				camera.translate( Qt.vector3d(translateX,translateY,0), Qt.TranslateViewCenter )

				if (translateWorldDelta!==undefined){
					if (translateWorldDelta.length()>0.0){
						camera.translateWorld(translateWorldDelta, Qt.TranslateViewCenter)
						translateWorldDelta = Qt.vector3d(0,0,0)
					}
				}
			}
        }
    ] // components
}
