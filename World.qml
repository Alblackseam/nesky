import QtQuick 2.0 as QQ2
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Logic 2.0
import Qt3D.Extras 2.0
import test 1.0


Entity {

    Camera {
        id: camera
        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        aspectRatio: width / height
		nearPlane: 1.0
		farPlane: 10000.0
		position: Qt.vector3d( 0, 0, 20 )
		upVector: Qt.vector3d( 0, 1, 0 )
		viewCenter: Qt.vector3d( 0, 0, 0 )
    }

	Layer{
		id: layer1
		//recursive: true
	}

	Entity {

	}

	components: [
		RenderSettings {
			activeFrameGraph: ForwardRenderer {
				camera: camera
				clearColor: Qt.rgba(0, 0, 0.05, 1)
			}
			renderPolicy: RenderSettings.Always
		},
//		RenderSettings {
//			activeFrameGraph: SimpleForwardRenderer {
//				//clearColor: Qt.rgba(0, 0, 0.03, 1)
//				camera: camera
//			}
//		},
		InputSettings {}
	]

	QCameraController{
		camera: camera
		helms: [shipFigherHelm, shipFigherHelm2]
	}


	SolarSystem{
	}

	Mesh
	{
		id: shipFighterMesh
		source: "qrc:/Resources/ShipFighter.obj"
	}

	NormalDiffuseMapMaterial
	{
		id: shipFighterMaterial
		ambient: Qt.rgba(0.1, 0.1, 0.1, 1)
		diffuse: TextureLoader { source: "qrc:/Resources/ShipFighter.jpg" }
		normal: TextureLoader { source: "qrc:/Resources/ShipFighterN.jpg" }
		//shininess: 0.1
		//specular: Qt.rgba(1,1,1,1)
	}

	Entity // ShipFighter
	{
		Transform
		{
			id: shipFighterTransform
			//rotation: fromAxisAndAngle(Qt.vector3d(1, 0, 0), -90)
		}

		components: [ shipFighterTransform, shipFighterMaterial, shipFighterMesh ]

		Entity {
			components: [
				GeometryRenderer
				{
					enabled: fireParticles.particleCount > 0
					instanceCount: fireParticles.particleCount
					indexOffset: 0
					firstInstance: 0
					primitiveType: GeometryRenderer.Patches
					verticesPerPatch: 1
					geometry: FireParticles
					{
						id: fireParticles
						sourceArea:      Qt.vector3d(0.0, -0.1, -6.0)
						destinationArea: Qt.vector3d(0.0, -0.1, -10.5) // -8.5
						emitting: shipFigherHelm.accelerate
						//emitting: true
					}
				}, // GeometryRenderer
				ParticleColorMaterial
				{
					//diffuse: "qrc:/Resources/Glowdot.png"
					diffuse: "qrc:/Resources/Flame-1.png"
					particleColor: Qt.vector4d(0.5, 0, 1, 1)
					particleSize: 3.0
				}
			]
		} // Fire

		HelmAir{
			id: shipFigherHelm
			place: QPlace{
				up: Qt.vector3d(0,0,1)
				front: Qt.vector3d(1,0,0)
				right: Qt.vector3d(0,1,0)
				//onRightChanged: console.log("right", right.x, right.y, right.z)
				//onFrontChanged: console.log("front", front.x, front.y, front.z)
			}
			shoter: shoter
			transform: shipFighterTransform
			cameraByPlace: false
		}
	} // ShipFighter



	Entity // ShipFighter
	{
		ObjectPicker {
			id: shipFighterPicker
			hoverEnabled: true
			dragEnabled: true

			// Explicitly require a middle click to have the Scene2D grab the mouse
			// events from the picker
			onPressed: {
				console.log("Picked", pick.button)
//				if (pick.button === PickEvent.MiddleButton) {
//					qmlTexture.mouseEnabled = !qmlTexture.mouseEnabled
//					logoControls.enabled = !logoControls.enabled
//				}
			}
		}

		Transform
		{
			id: shipFighterTransform2
			//rotation: fromAxisAndAngle(Qt.vector3d(1, 0, 0), -90)
		}

		components: [ shipFighterTransform2, shipFighterMaterial, shipFighterMesh, shipFighterPicker ]

		HelmAir{
			id: shipFigherHelm2
			place: QPlace{
				pos: Qt.vector3d(3,3,3)
				up: Qt.vector3d(0,0,1)
				front: Qt.vector3d(1,0,0)
				right: Qt.vector3d(0,1,0)
				//onRightChanged: console.log("right", right.x, right.y, right.z)
				//onFrontChanged: console.log("front", front.x, front.y, front.z)
			}
			shoter: shoter
			transform: shipFighterTransform2
			cameraByPlace: false
		}
	} // ShipFighter





	// Shoter
	Entity {
		Shoter{
			id: shoter
			QQ2.Component.onCompleted: {
				shoter.bulletsGeometry = bulletsGeometry
			}
		}

		GeometryRenderer{
			id: bulletsRenderer
			instanceCount: bulletsGeometry.bulletsCount
			indexOffset: 0
			firstInstance: 0
			primitiveType: GeometryRenderer.Triangles
			geometry: BulletsGeometry {
				id: bulletsGeometry
				QQ2.Component.onCompleted: createBoundingBox()

				onBoundingVolumePositionAttributeChanged: {
					console.log("onBoundingVolumePositionAttributeChanged")
				}
			}
		}

		BulletsMaterial{
			id: bulletsMaterial
			ambient:  Qt.rgba( 1, 0.0, 0.0, 1.0 )
			diffuse:  Qt.rgba( 1, 0.0, 0.0, 1.0 )
		}

		components: [bulletsRenderer, bulletsMaterial]
	}

	// H2F2
	Entity {
		components: [
			Mesh { source: "qrc:/Resources/H2F2.obj" },
			//Mesh { source: "qrc:/Resources/StarShip.obj" },
			//Mesh { source: "qrc:/Resources/AxisPiramid.obj" },
			//Mesh { source: "qrc:/Resources/ShipFighter.obj" },
			DiffuseMapMaterial {
				ambient: Qt.rgba(0.1, 0.1, 0.1, 1)
				diffuse: TextureLoader { source: "qrc:/Resources/H2F2_mat.png" }
				//shininess: 0.1
				//specular: Qt.rgba(1,1,1,1)
			},
			Transform {
				translation: Qt.vector3d(0, -10, 0)
				rotation: fromAxisAndAngle(Qt.vector3d(1, 0, 0), 90.0)
			}
		]
	}

	// SciFi_MK6
	Entity {
		components: [
			Mesh { source: "qrc:/Resources/SciFi_Fighter-MK6.obj" },
			DiffuseMapMaterial {
				ambient: Qt.rgba(0.2, 0.2, 0.2, 1)
				diffuse: TextureLoader { source: "qrc:/Resources/SciFi_Fighter-MK6-diffuse.jpg" }
				shininess: 0.1
				//specular: Qt.rgba(1,1,1,1)
			},
			Transform {
				translation: Qt.vector3d(10, -20, 0)
			}
		]
	}

	FrameAction {
		onTriggered: {
			//console.log("123", pointsMesh.geometry.boundingVolumePositionAttribute)
			shoter.update(dt)
			fireParticles.update(dt)
		}
	}
}
