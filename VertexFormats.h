#pragma once

#include <deque>
#include <tuple>
#include <utility>

#include "Qt3DRender/QAttribute"
#include "GeometryAttributes.h"




struct VertexColorQVector3D
{
	using PositionAttribute = Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3>;
	using ColorAttribute = Attribute<QVector4D, Qt3DRender::QAttribute::Float, 4>;
	using Attributes = std::tuple<
		PositionAttribute,
		ColorAttribute
	>;
	static constexpr auto BufferSize = attributesStrideSize<Attributes>();

	QVector3D position;
	QVector4D color;

	auto toBuffer() const {
		return PositionAttribute::buffer(position) + ColorAttribute::buffer(color);
	}

	static inline auto getAttibutes()
	{
		return std::make_tuple(PositionAttribute{QString::fromLocal8Bit("vertexPosition")}
			, ColorAttribute{QString::fromLocal8Bit("vertexColor")});
	}
};



struct ParticleVertex_PositionColorRotation
{
	using PositionAttribute = Attribute<QVector3D, Qt3DRender::QAttribute::Float, 3>;
	using ColorAttribute = Attribute<QVector4D, Qt3DRender::QAttribute::Float, 4>;
	using RotationAttribute = Attribute<float, Qt3DRender::QAttribute::Float, 1>;
	using SizeAttribute = Attribute<float, Qt3DRender::QAttribute::Float, 1>;

	using Attributes = std::tuple<
		PositionAttribute,
		ColorAttribute,
		RotationAttribute,
		SizeAttribute
	>;
	static constexpr auto BufferSize = attributesStrideSize<Attributes>();

	QVector3D position;
	QVector4D color;
#pragma pack(push, 1)
	float rotation{};
	float size{1};
#pragma pack(pop)

	auto toBuffer() const
	{
		return PositionAttribute::buffer(position) + ColorAttribute::buffer(color) + RotationAttribute::buffer(rotation) + SizeAttribute::buffer(size);
	}

	static inline auto getAttibutes()
	{
		return std::make_tuple(PositionAttribute{QString::fromLocal8Bit("vertexPosition")}
			, ColorAttribute{QString::fromLocal8Bit("vertexColor")}
			, RotationAttribute{QString::fromLocal8Bit("vertexRotation")}
			, SizeAttribute{QString::fromLocal8Bit("vertexSize")}
			);
	}
};


static_assert(sizeof(ParticleVertex_PositionColorRotation) == sizeof(float) * 9);
