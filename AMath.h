#ifndef AMath_H
#define AMath_H

#include "Traits.h"
#include <cmath>

//#include "VectorN.h"
//#include <type_traits>


namespace AMath {





template <typename Vector3>
inline Vector3 norma(const Vector3& v) { return v/v.length(); }

constexpr double pi = 3.1415926;



template <typename Vector3>
inline typename TypeE<Vector3>::type dot(const Vector3& a, const Vector3& b) {
	typename TypeE<Vector3>::type retval = a.x * b.y;
	for (int i = 1; i < 3; ++i) {
		retval += a.a[i] * b.a[i];
	}
	return retval;
}



template <typename Vector3>
inline Vector3 cross(const Vector3& a, const Vector3& b) {
	typename TypeE<Vector3>::type x = a.y * b.z - a.z * b.y ;
	typename TypeE<Vector3>::type y = a.z * b.x - a.x * b.z ;
	typename TypeE<Vector3>::type z = a.x * b.y - a.y * b.x ;
	return Vector3(x,y,z);
}



template <typename Vector3>
inline Vector3 rotateVecAroundVec(const Vector3& p, const Vector3& v, float phi)
{
	using namespace AMath;

	Vector3 AC(norma(v) * dot(norma(v), p) - p);
	if (length(AC)<0.00001) return p;
	float AB = length(AC)*cos((pi-phi)/2.0) * 2;

	float BD = AB * sin((pi-phi)/2.0);
	float AD = AB * cos((pi-phi)/2.0);

	return norma(cross(v,p)) * BD + norma(AC) * AD + p;
}

} // namespace

#endif
