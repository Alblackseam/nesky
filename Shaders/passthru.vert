#version 400 core
#pragma include VertexFormats.inc.vert

in vec3 vertexPosition;
in vec4 vertexColor;
in float vertexRotation;
in float vertexSize;

//uniform mat4 modelMatrix;
//uniform mat4 mvp;


//out vec4 vs_out;
out ParticleVertex vs_out;


void main()
{
	gl_Position = vec4( vertexPosition, 1.0 );
	vs_out.color = vertexColor;
	vs_out.rotation = vertexRotation;
	vs_out.size = vertexSize;
}

