#pragma once

#include <deque>
#include <tuple>
#include <utility>
#include <random>

#include <QCylinderGeometry>
#include "Qt3DRender/QAttribute"
#include "GeometryAttributes.h"

#include "VertexFormats.h"



struct FireParticle: public VertexColorQVector3D
{
	static constexpr float lifeTime = 0.2f; // seconds

	FireParticle(QVector3D source, QVector3D target)
		: VertexColorQVector3D{source, QVector4D(0.5, 0, 1, 1)}
		, _target{ target }
		, _speed{ (_target - source).length() / lifeTime }
	{
	}

	QVector3D _target;
	float _speed;
	float time{};

	void update(float deltaTime){
		time += deltaTime;

		QVector3D delta = deltaTime * (_target - position).normalized() * _speed;
		position += delta;
	}

	bool isOutDated() const {
		return time > lifeTime;
	}

};



struct FireParticles : public Qt3DRender::QGeometry
{
	Q_OBJECT
	Q_PROPERTY(QVector3D sourceArea MEMBER sourceArea NOTIFY sourceAreaChanged)
	Q_PROPERTY(QVector3D destinationArea MEMBER destinationArea NOTIFY destinationAreaChanged)
	Q_PROPERTY(bool emitting MEMBER emitting)
	Q_PROPERTY(int particleCount MEMBER particleCount NOTIFY particleCountChanged)

public:
	FireParticles(QNode *parent = nullptr)
		: Qt3DRender::QGeometry{parent}
		, _buffer{parent}
	{
		_buffer.setType(Qt3DRender::QBuffer::VertexBuffer);

		_attributes = convertToQAttributes(VertexColorQVector3D::getAttibutes(), parent);
		qDebug() << attributes().size();
		for (auto& attribute: _attributes)
			addAttribute(attribute);
		qDebug() << attributes().size();
	}



	template<typename Vertices>
	void updateBuffer(const Vertices& vertexData){
		auto bufferData = vertexDataToQByteArray(vertexData);
		_buffer.setData(bufferData);

		for (auto& attribute: _attributes){
			attribute->setBuffer(&_buffer);
			attribute->setCount(vertexData.size());
		}

		if (particleCount != vertexData.size())
		{
			particleCount = vertexData.size();
			emit particleCountChanged(particleCount);
		}
	}

signals:
	void particleCountChanged(int);
	void sourceAreaChanged();
	void destinationAreaChanged();



public slots:
	void emitParticle(){
		QVector3D pos(sourceArea.x() + range05(mt), sourceArea.y() + range05(mt), sourceArea.z() + range05(mt));
		QVector3D target(destinationArea.x() + range01(mt), destinationArea.y() + range01(mt), destinationArea.z() + range01(mt));

		_bullets.push_front(FireParticle(pos, target));

	}


	void update(float deltaTime){
		if (emitting) {
			if (deltaTime * emitSpeed > dist(mt))
				emitParticle();
		}

		for (auto bullet = _bullets.begin(); bullet != _bullets.end();) {
			bullet->update(deltaTime);

			if (bullet->isOutDated())
				bullet = _bullets.erase(bullet);
			else
				++bullet;
		}

		//if (_bullets.size() > 0)
		updateBuffer(_bullets);
	}

private:
	static constexpr float emitSpeed = 100;


	bool emitting = true;
	QVector3D sourceArea{};
	QVector3D destinationArea{};
	int particleCount = 0;
	std::deque<FireParticle> _bullets;

	std::random_device rd{};
	std::mt19937 mt{ rd() };
	std::uniform_real_distribution<double> dist{ 0.0, 1.0 };
	std::uniform_real_distribution<double> range05{ -0.5, 0.5 };
	std::uniform_real_distribution<double> range01{ -0.05, 0.05 };

	Qt3DRender::QBuffer _buffer;
	std::vector<Qt3DRender::QAttribute*> _attributes;
};





