#ifndef QMath_H
#define QMath_H

#include "Traits.h"
#include <QVector3D>

namespace AMath {

template<>
struct TypeE<QVector3D>{
	typedef float type;
};



inline float length(const QVector3D& v) { return v.length(); }


inline float dot(const QVector3D& a, const QVector3D& b) {
	float retval = a.x() * b.y();
	for (int i = 1; i < 3; ++i) {
		retval += a[i] * b[i];
	}
	return retval;
}



inline QVector3D cross(const QVector3D& a, const QVector3D& b) {
	float x = a.y() * b.z() - a.z() * b.y();
	float y = a.z() * b.x() - a.x() * b.z();
	float z = a.x() * b.y() - a.y() * b.x();
	return QVector3D(x,y,z);
}

}

#endif
