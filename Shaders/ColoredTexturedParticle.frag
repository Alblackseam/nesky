#version 400 core

#pragma include VertexFormats.inc.vert


uniform sampler2D diffuseTexture;
uniform vec4 color;


in WireframeVertex {
	vec2 texCoord;
	vec4 color;
} fs_in;

//in WireframeVertex fs_in;



out vec4 fragColor;
const float alpha_threshold = 0.01;


void main()
{
	vec4 diffuseTextureColor = texture( diffuseTexture, fs_in.texCoord ).rgba;

	if(diffuseTextureColor.r <= alpha_threshold)
		discard;

	fragColor = fs_in.color * diffuseTextureColor.r;
}
