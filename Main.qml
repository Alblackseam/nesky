import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Scene3D 2.0

Item {
    id: mainview
    width: 1280
    height: 768
    visible: true

    Scene3D {
        anchors.fill: parent
        aspects: ["render", "logic", "input"]
		focus: true
		World { id: solarsystem }
    }

    Text {
        text: "Shift - accelerate, Space - shot, RightLeft - rotate, Mouse Whieel - zoom, Holding MouseMidButton - move screen"
        color: "white"
    }
}


/*
    uniform variables (found in renderview.cpp)
        modelMatrix
        viewMatrix
        projectionMatrix
        modelView
        viewProjectionMatrix
        modelViewProjection
        mvp
        inverseModelMatrix
        inverseViewMatrix
        inverseProjectionMatrix
        inverseModelView
        inverseViewProjectionMatrix
        inverseModelViewProjection
        modelNormalMatrix
        modelViewNormal
        viewportMatrix
        inverseViewportMatrix
        exposure
        gamma
        time
        eyePosition
    attributes (found in qattribute.cpp)
        vertexPosition
        vertexNormal
        vertexColor
        vertexTexCoord
        vertexTangent
*/

