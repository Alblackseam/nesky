#pragma once

#include <deque>
#include <tuple>
#include <utility>

#include <QCylinderGeometry>
#include "Qt3DRender/QAttribute"
#include "GeometryAttributes.h"
#include "BulletsGeometry.h"


struct Bullet{
	Place<QVector3D> place;
	//QMatrix4x4 place;
	//QVector3D position;
	//QVector3D velosity;

	float time{};

	void update(float deltaTime){
		static constexpr float velocity = 30.0f;
		time += deltaTime;
		place.onward(deltaTime * velocity);
	}

	bool isOutDated() const {
		return time > 5.0f;
	}
};



////////////////////// BufferGeometry.h





struct Shoter : public QObject//  , public Qt3DRender::QGeometryRenderer
{
	Q_OBJECT
	Q_PROPERTY(BulletsGeometry* bulletsGeometry MEMBER bulletsGeometry) // NOTIFY bulletGeometryChanged())

public slots:
	void shot(QPlace* place){
		//qDebug() << place;

		//QMatrix4x4 rotateScale = place;
		//rotateScale.rotate(90.0, QVector3D(0,1,0));
		//QMatrix4x4 m(rotateScale.toGenericMatrix<4,4>());

		Bullet b{*place};
		b.place.onward(5.1f);
		_bullets.push_back(b);
	}

	void update(float deltaTime){
		for (auto bullet = _bullets.begin(); bullet != _bullets.end();) {
			bullet->update(deltaTime);

			if (bullet->isOutDated())
				bullet = _bullets.erase(bullet);
			else
				++bullet;
		}

		//if (_bullets.size() > 0)
		updateGeometry();
	}

	void updateGeometry(){
		std::vector<Vertex> bulletVertices;
		for (const auto& bullet: _bullets){
			//bulletVertices.push_back(Vertex{ bullet.position, bullet.velosity, QColor{1,0,0,1}} );
			bulletVertices.push_back(Vertex{ bullet.place.worldMatrix<QMatrix4x4>() } );
		}

		if (bulletsGeometry)
			bulletsGeometry->updateBuffer(bulletVertices);
	}

private:
	std::deque<Bullet> _bullets;
	BulletsGeometry* bulletsGeometry = nullptr;
};





