#pragma once

#include <deque>
#include <tuple>
#include <utility>
#include <random>

#include <QCylinderGeometry>
#include "Qt3DRender/QAttribute"
#include "GeometryAttributes.h"

#include "VertexFormats.h"



struct StarParticle: public ParticleVertex_PositionColorRotation
{
	StarParticle()
		: ParticleVertex_PositionColorRotation{}
	{
	}

	void update(float deltaTime){
		rotation += 0.001f * deltaTime;
	}
};



struct Stars : public Qt3DRender::QGeometry
{
	Q_OBJECT

public:
	Stars(QNode *parent = nullptr)
		: Qt3DRender::QGeometry{parent}
		, _buffer{parent}
	{
		_buffer.setType(Qt3DRender::QBuffer::VertexBuffer);

		_attributes = convertToQAttributes(StarParticle::getAttibutes(), parent);
		qDebug() << attributes().size();
		for (auto& attribute: _attributes)
			addAttribute(attribute);
		qDebug() << attributes().size();
	}


	template<typename Vertices>
	void updateBuffer(const Vertices& vertexData){
		auto bufferData = vertexDataToQByteArray(vertexData);
		_buffer.setData(bufferData);

		for (auto& attribute: _attributes){
			attribute->setBuffer(&_buffer);
			attribute->setCount(vertexData.size());
		}
	}

public slots:
	void update(float deltaTime){
		if (_stars.size() == 0) {
			generateStars();
		} else {
			updateStars(deltaTime);
		}
		updateBuffer(_stars);
	}

private:
	std::deque<StarParticle> _stars;

	std::random_device rd{};
	std::mt19937 mt{ rd() };
	std::uniform_real_distribution<double> rangeColor{ 0.0, 1.0 };
	std::uniform_real_distribution<double> rangeStarSize{ 5, 50 };
	std::uniform_real_distribution<double> rangeStarXy{ -10000, 10000 };
	std::uniform_real_distribution<double> rangeStarZ{ -20, -10 };

	Qt3DRender::QBuffer _buffer;
	std::vector<Qt3DRender::QAttribute*> _attributes;
	float time{};
	int updateStarsIteration{};

	void generateStars(){
		for (int i = 0; i < 10000; ++i) {
			StarParticle sp;
			sp.position = QVector3D(rangeStarXy(mt), rangeStarXy(mt), rangeStarZ(mt) * 100);
			sp.color = QVector4D(rangeColor(mt), rangeColor(mt), rangeColor(mt), rangeColor(mt));
			sp.rotation = 1;
			sp.size = rangeStarSize(mt);

			_stars.push_back(sp);
		}
	}

	void updateStars(float deltaTime){
		time += deltaTime;
		auto updateInterval = 0.3f;

		if ((int)(time / updateInterval) > updateStarsIteration){
			updateStarsIteration = time / updateInterval;

			for (int i = 0; i < 10000; ++i) {
				StarParticle& sp = _stars[i];
				//sp.position = QVector3D(rangeStarXy(mt), rangeStarXy(mt), rangeStarZ(mt) * 100);
				//sp.color = QVector4D(range1(mt), range1(mt), range1(mt), range1(mt));
				sp.size = rangeStarSize(mt);
			}
		}
	}

};





