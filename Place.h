#ifndef Place_H
#define Place_H

#include "QMath.h"
#include "AMath.h"
//#include "VectorN.h"

//#include "AMatrix4.h"

template<typename Vector3>
struct Place
{
	//typedef NettleMath::vectorN<float, 3> Vector3;
    Vector3 r; // Right X
	Vector3 u; // Up Y
	Vector3 f; // Front Z
	Vector3 p; // Position
	float scale{1.0f};
	
	void setUp( const Vector3& Up ){ u=Up; }
	void setFront( const Vector3& Front ){ f=Front; }
	void setPos(const Vector3& Position ){ p=Position; }
	//void setRight( const Vector3& Right ){ l=Right; }

    void calcRight(){ r = cross(u,f); } // By Up and Front
	//void calcUp(); // By Front and Right
	//void calcFront(); // By Up and Right
	
	

	Vector3 getPos()   const { return p; }
	Vector3 getFront() const { return f; }
	Vector3 getRight() const { return r; }
	Vector3 getUp()    const { return u; }

	Place()
        : r(Vector3(1,0,0))
		, u(Vector3(0,1,0))
		, f(Vector3(0,0,1))
		, p(0,0,0) {
	}

	Place(const Vector3& position )
        : r(Vector3(1,0,0))
		, u(Vector3(0,1,0))
		, f(Vector3(0,0,1))
		, p(position) {
	}

	Place(const Vector3& position, const Vector3& Up, const Vector3& Front, const Vector3& left )
        : r(left)
		, u(Up)
		, f(Front)
		, p(position) {
	}

	void turnUp(float phi) {
        f = AMath::rotateVecAroundVec(f, r, -phi);
		f.normalize();
        u = AMath::cross(f, r);
		u.normalize();
	}

	void turnDown(float phi) {
        f = AMath::rotateVecAroundVec(f, r, phi);
		f.normalize();
        u = AMath::cross(f, r);
		u.normalize();
	}

	void turnLeft(float phi, const Vector3& up) {
		f = AMath::rotateVecAroundVec(f, up,  phi);   // V
		f.normalize();
		u = AMath::rotateVecAroundVec(u, up,  phi);   // V
		u.normalize();
        r = AMath::cross(u, f);
        r.normalize();
	}

	void turnLeft(float phi) {
		f = AMath::rotateVecAroundVec(f, u,  phi);   // V
		f.normalize();
        r = AMath::cross(u, f);
        r.normalize();
	}

	void turnRight(float phi) {
		f = AMath::rotateVecAroundVec(f, u, -phi);    // V
		f.normalize();
        r = AMath::cross(u, f);
        r.normalize();
	}


	void rotateAroundY(float phi) {
		u.normalize();
		f = AMath::rotateVecAroundVec(f, u, phi);
		f.normalize();
		r = AMath::cross(u, f);
	}



	void roll(float phi) {
		u = AMath::rotateVecAroundVec(u, f, phi);
		u.normalize();
        r = AMath::cross(u, f);
        r.normalize();
	}


	void turnAroundVector(const Vector3& v, float phi) {
		f = AMath::rotateVecAroundVec(f, v, -phi);
		u = AMath::rotateVecAroundVec(u, v, -phi);
		f.normalize();
		u.normalize();
        r = AMath::cross(u, f);
	}

	void onward(float len){	     p+= AMath::norma(f)*len; }
	void backward(float len){    p-= AMath::norma(f)*len; }
    void strafeLeft(float len){  p+= AMath::norma(r)*len; }
    void strafeRight(float len){ p-= AMath::norma(r)*len; }
	void strafeUp(float len){    p+= AMath::norma(u)*len; }
	void strafeDown(float len){  p-= AMath::norma(u)*len; }


	template<typename Matrix4>
	Matrix4 worldMatrix() const {
		auto rScaled = r * scale;
		auto uScaled = u * scale;
		auto fScaled = f * scale;
		return Matrix4(
			rScaled.x(),  uScaled.x(),  fScaled.x(),  p.x(),
			rScaled.y(),  uScaled.y(),  fScaled.y(),  p.y(),
			rScaled.z(),  uScaled.z(),  fScaled.z(),  p.z(),
			0, 0, 0, 1 );
	}

	template<typename Matrix4>
	Matrix4 worldMatrixOld() const {
		return Matrix4(
             r.x(),  r.y(),  r.z(),  p.x(),
			 u.x(),  u.y(),  u.z(),  p.y(),
			 f.x(),  f.y(),  f.z(),  p.z(),
			0, 0, 0, 1 );
	}

	/*
	void fromWorldMatrix(const Matrix4& m) {
		R.SetCoord(m.M11, m.M12, m.M13);
		F.SetCoord(m.M21, m.M22, m.M23);
		U.SetCoord(m.M31, m.M32, m.M33);
		P.SetCoord(m.M14, m.M24, m.M34);
	}*/

	/*
	std::string debugString() const {
		std::stringstream ss;
		ss << "P:" << P.debugString() << " U:" << U.debugString() << " F:" << F.debugString() << " R:" << R.debugString();
		return ss.str();
	}*/
};

#endif
