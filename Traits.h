#ifndef Traits_H
#define Traits_H

#include <type_traits>

namespace AMath {


template<typename>
struct TypeE;

template<typename T>
struct TypeE{
	typedef typename T::ElementType type;
};


} // namespace

#endif
