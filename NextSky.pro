QT += qml quick
QT += 3dcore 3drender 3dinput 3dquick 3dquickrender 3dquickinput 3dquickextras

# CONFIG += resources_big c++11
CONFIG += c++1z

# -std=c++1z  -std=gnu++1z


HEADERS += \
	AMath.h \
	Place.h \
	QMath.h \
	QPlace.h \
	Traits.h \
	VectorN.h \
	Shoter.h \
	FireParticles.h \
	Stars.h \
	VertexFormats.h \
	BulletsGeometry.h


SOURCES += \
    main.cpp

OTHER_FILES += \
    *.qml \
    Shaders/gl3/*.* \
    Shaders/es2/*.* \

RESOURCES += NextSky.qrc


# DISTFILES += \
#    PlanetsMain.qml \
#    android/AndroidManifest.xml \
# ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
