
/*
* Nettle Source File.
* Copyright (C) 2013, Nettle (http://nttl.ru/).
*
* This file is part of NettleMath.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the GNU Lesser General Public License
* (LGPL) version 2.1 which accompanies this distribution, and is available at
* http://www.gnu.org/licenses/lgpl-2.1.html
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* Description:
*
* History:
* Created by Sergey Serb, l-proger@yandex.ru
*/

#ifndef NVectorBase_h__
#define NVectorBase_h__

#include <cmath>
#include <cassert>
#include <limits>
#include <initializer_list>
#include <iostream>
#include <utility>
#include <array>
#include <type_traits>
#include <typeinfo>
//#include <xstddef>
#include "NettleMathHelpers.h"





namespace NettleMath{

#define NOT_VECTOR(T) T, typename = std::enable_if_t<!std::is_base_of<vectorNBase,T>::value>
#define REMOVE_CONST_REF(T) std::remove_const<std::remove_reference<T>::type>::type>
#define MUL_TYPE(TA,TB) decltype(std::declval<TA>()*std::declval<TB>())



	struct vectorNBase {};

	template<typename T, int N>
	struct vectorNfields : public vectorNBase {
		std::array<T,N> a;
		constexpr vectorNfields() : a{} {}
		constexpr vectorNfields(const std::array<T, N>& values) : a(values) {}

        constexpr const T& operator[](size_t i) const { return a[i]; }
        T& operator[](size_t i) { return a[i]; }
	};


	template<typename T>
	struct vectorNfields<T,2> : public vectorNBase {
		T x;
		T y;
		constexpr vectorNfields() : x{}, y{} {}
		constexpr vectorNfields(const std::array<T, 2>& values) : x{ values[0] }, y{ values[1] } {}

        constexpr const T& operator[](size_t i) const { return (&x)[i]; }
        T& operator[](size_t i) { return (&x)[i];  }
	};

	template<typename T>
	struct vectorNfields<T, 3> : public vectorNBase {
		T x;
		T y;
		T z;
		constexpr vectorNfields() : x{}, y{}, z{} {}
		//constexpr vectorNfields(T ax, T ay, T az) : x{ax} , y{ay}, z{az} {}
		constexpr vectorNfields(const std::array<T, 3>& values) : x{ values[0] }, y{ values[1] }, z{ values[2] } {}

        constexpr const T& operator[](size_t i) const { return (&x)[i]; }
        T& operator[](size_t i) { return (&x)[i];  }
	};

	template<typename T>
	struct vectorNfields<T, 4> : public vectorNBase {
		T x;
		T y;
		T z;
		T w;
		constexpr vectorNfields() : x{}, y{}, z{}, w{} {}
		//constexpr vectorNfields(T ax, T ay, T az) : x{ax} , y{ay}, z{az} {}
		constexpr vectorNfields(const std::array<T, 4>& values) : x{ values[0] }, y{ values[1] }, z{ values[2] }, w{ values[3] } {}

        constexpr const T& operator[](size_t i) const { return (&x)[i]; }
        T& operator[](size_t i) { return (&x)[i];  }
	};




	//Вектор
	template<typename T, int N>
	struct vectorN: public vectorNfields<T,N> {
		typedef T ElementType;
		static constexpr size_t size = N;


		constexpr vectorN() : vectorNfields<T,N>() {}

		constexpr vectorN(const std::array<T,N>& value) : vectorNfields<T,N>(value) {}

		template <typename TA, typename std::enable_if<!std::is_base_of<vectorNBase,TA>::value,int>::type = 0
			//, typename = std::enable_if_t<std::is_same<T,MUL_TYPE(T,TA)>::value> // Устранить потерю точности vec<float> from vec<double>
			//, typename = std::enable_if_t<std::is_same<T,typename NettleMath::MultiType<T,TA>::type>::value> // Устранить потерю точности vec<float> from vec<double>
		>
		constexpr vectorN(const TA& value)
			: vectorNfields<T,N>(NettleMath::ConstValueArray<N>::getArray((T)value)) {
		}

		//template <typename TA> constexpr vectorN(const std::array<TA,N>& value) : vectorNfields<T,N>(value) {}


		template <typename TF, typename ... TArgs
			, typename std::enable_if<(sizeof...(TArgs)==(N-1)), int>::type = 0
			, typename std::enable_if<std::is_same<T,typename MultiType<T,TArgs...>::type>::value,int>::type = 0> // float -> double
		constexpr vectorN(const TF& argf, const TArgs&... args)
			: vectorN<T, N>(makeArrayFromValues<T, TF, TArgs...>(argf, args...)) {
		}

		template <typename TF, typename ... TArgs
			, typename std::enable_if<(sizeof...(TArgs)==(N-1)), int>::type = 0
			, typename std::enable_if<!std::is_same<T,typename MultiType<T,TArgs...>::type>::value,int>::type = 0> // double -> float
		explicit constexpr vectorN(const TF& f, const TArgs&... args)
			: vectorN<T, N>(makeArrayFromValues<T, TF, TArgs...>(f, args...)) {
		}



		//template<typename TA, typename = std::enable_if_t<std::is_same<T,MUL_TYPE(T,TA)>::value>>
		//vectorN(const vectorN<TA,N>& val){ for (size_t i = 0; i < size; i++) at[i] = T(val[i]); }


		template <typename TA, int NA
			, typename std::enable_if<std::is_same<T,typename NettleMath::MultiType<T,TA>::type>::value,int>::type = 0> // implicit vec<double> from vec<float>
		vectorN(const vectorN<TA, NA>& val) {
			constexpr int NMin = N<NA ? N : NA;
			for (size_t i = 0; i < NMin; i++) at(i) = (T)val[i];
			for (size_t i = NMin; i < N; i++) at(i) = T{};
		}


		template <typename TA, typename std::enable_if<!std::is_same<T,typename NettleMath::MultiType<T,TA>::type>::value,int>::type = 0> // vec<float> from vec<double>
		explicit constexpr vectorN(const vectorN<TA,N>& val)
			: vectorNfields<T,N>(make_array<N>(CopyFromContainerHelper<T>(val))) {
			//for (size_t i = 0; i < N; i++) at(i) = (T)val[i];
		}


		template<typename NOT_VECTOR(TA)>
        inline vectorN& operator = (const TA& v){
			for (size_t i = 0; i < size; ++i) at(i) = (T)v;
			//for (int i = 0; i < size; ++i) { at(i).~T(); new(&at(i)) T(v); }
			return *this;
		}


		template <typename TA, int NA
			, typename std::enable_if<std::is_same<T,typename NettleMath::MultiType<T,TA>::type>::value,int>::type = 0> // implicit vec<double> from vec<float>
        inline vectorN& operator =(const vectorN<TA, NA>& val) {
			constexpr int NMin = N<NA ? N : NA;
			for (size_t i = 0; i < NMin; i++) at(i) = (T)val[i];
			for (size_t i = NMin; i < N; i++) at(i) = T{};
			return *this;
		}


		template<typename TA>
		inline explicit operator vectorN<TA, N>() const {
			vectorN<TA, N> result;
			for (size_t i = 0; i < size; i++)
				result[i] = (TA)at(i);
			return result;
		}


		
        inline T length() const {
			return sqrt(lengthSquared());
		}


        inline T lengthSquared() const {
			T sum = at(0) * at(0);
			for (size_t i = 1; i < size; i++) {
				sum += at(i) * at(i);
			}
			return sum;
		}


        inline void normalize() {
			T normalizer = length();
			for(size_t i = 0; i < size; i++){
				at(i) /= normalizer;
			}
        }


        inline constexpr const T& operator[](size_t i) const {
			//qDebug() << "[]" << i;
			return vectorNfields<T,N>::operator [](i);
		}

        inline T& operator[](size_t i) {
			//qDebug() << "[]" << i;
			return vectorNfields<T,N>::operator [](i);
		}

        inline constexpr const T& at(size_t i) const { return vectorNfields<T,N>::operator[](i); }
        inline T& at(size_t i) { return vectorNfields<T,N>::operator[](i); }


		template<typename Arg,
			typename TypeResult = std::result_of_t<T(Arg)>>
		auto operator()(const Arg& arg) const -> NettleMath::vectorN<TypeResult, N>
		{
			NettleMath::vectorN<TypeResult, N> res;
			for (int i=0; i<N; i++)
				res[i] = at(i)(arg);
			return res;
		}


		/*
        const T& operator [] (int index) const {
#if defined(NMATH_DEBUG)
            assert((index >= 0) && (index < size));
#endif
			return operator[][index];
        }

        T& operator [] (int index) {
#if defined(NMATH_DEBUG)
            assert((index >= 0) && (index < size));
#endif
			return operator[](index);
		}*/

        /*operator const T* () const{
            return reinterpret_cast<const T*>(this);
        }

        operator T* (){
            return reinterpret_cast<T*>(this);
        }*/


        inline vectorN<T,N> operator -() const {
			vectorN<T,N> vec;
			for (size_t i = 0; i < size; ++i)
				vec[i] = -at(i);
			return vec;
		}

		

		bool Any() const {
			for (size_t i = 0; i < size; ++i) {
				if (at(i)) return true;
			}
			return false;
		}

		bool All() const {
			for (size_t i = 0; i < size; ++i) {
				if (!at(i)) return false;
			}
			return true;
		}
		
		T maxElement() const {
			T maxElem = at(0);
			for (size_t i=1; i<size; i++)
				if(at(i)>maxElem)
					maxElem = at(i);
			return maxElem;
		}


#define AR_OPERATOR_vectorN(op)\
	template<typename TR, typename R = decltype(std::declval<T>() op std::declval<TR>())>\
    inline vectorN<R, N> operator op (const vectorN<TR, N>& v) const {\
		vectorN<R, N> retval;\
		for (size_t i = 0; i < size; ++i) retval[i] = at(i) op v[i];\
        return retval;\
    }\
    template<typename NOT_VECTOR(TR), typename R = decltype(std::declval<T>() op std::declval<TR>())>\
    inline vectorN<R, N> operator op (const TR& v) const {\
		vectorN<R, N> retval;\
		for (size_t i = 0; i < size; ++i) retval[i] = at(i) op v;\
        return retval;\
    }\
	template<typename NOT_VECTOR(TA)\
		, typename = decltype(std::declval<T>()*std::declval<TA>())>\
    inline vectorN operator op##= (const TA& v){\
		for (size_t i = 0; i < size; ++i) at(i) op##= v;\
        return *this;\
    }\
    inline vectorN operator op##= (const vectorN& v) {\
		for (size_t i = 0; i < size; ++i) at(i) op##= v[i]; \
        return *this;\
    }
		
		AR_OPERATOR_vectorN(+)
		AR_OPERATOR_vectorN(-)
		AR_OPERATOR_vectorN(*)
		AR_OPERATOR_vectorN(/)


#define LOGIC_OPERATOR(op)\
		template<typename TR>\
		vectorN<bool, N> operator op (const vectorN<TR, N>& v) const{\
			vectorN<bool, N> retval;\
			for (int i = 0; i < size; ++i) {\
				retval[i] = at(i) op v[i];\
			}\
			return retval;\
		}

		LOGIC_OPERATOR(<)
		LOGIC_OPERATOR(>)
		LOGIC_OPERATOR(== )
		LOGIC_OPERATOR(!= )


		//template<typename = std::enable_if_t<(N==2)>> static constexpr NettleMath::vectorN<T,N> theAxisX() { return vectorN<T,N>(1,0); }
		//template<typename = std::enable_if_t<(N==3)>> static constexpr NettleMath::vectorN<T,N> theAxisX() { return vectorN<T,N>(1,0,0); }

		template<typename TA, int Dimension, typename std::enable_if<(Dimension==2),int>::type = 0>
		static constexpr NettleMath::vectorN<TA,Dimension> axisXtemplate() { return vectorN<TA,Dimension>(1,0); }

		template<typename TA, int Dimension, typename std::enable_if<(Dimension==3),int>::type = 0>
		static constexpr NettleMath::vectorN<TA,Dimension> axisXtemplate() { return vectorN<TA,Dimension>(1,0,0); }

		template<typename TA, int Dimension, typename std::enable_if<(Dimension==2),int>::type = 0>
		static constexpr NettleMath::vectorN<TA,Dimension> axisYtemplate() { return vectorN<TA,Dimension>(0,1); }

		template<typename TA, int Dimension, typename std::enable_if<(Dimension==3),int>::type = 0>
		static constexpr NettleMath::vectorN<TA,Dimension> axisYtemplate() { return vectorN<TA,Dimension>(0,1,0); }

		template<typename TA, int Dimension, typename std::enable_if<(Dimension==3),int>::type = 0>
		static constexpr NettleMath::vectorN<TA,Dimension> axisZtemplate() { return vectorN<TA,Dimension>(0,0,1); }


		static constexpr NettleMath::vectorN<T,N> axisX() { return vectorN<T,N>::axisXtemplate<T,N>(); }
		static constexpr NettleMath::vectorN<T,N> axisY() { return vectorN<T,N>::axisYtemplate<T,N>(); }
		static constexpr NettleMath::vectorN<T,N> axisZ() { return vectorN<T,N>::axisZtemplate<T,N>(); }





		friend std::ostream& operator << (std::ostream& os, const vectorN& v)
		{
			os << typeid(T).name() << size << "(" << v[0];
			for (int i = 1; i < size; ++i) {
				os << "," << v[i];
			}			
			os << ")";
			return os;
		}


	}; // struct VectorN




	/*
	template<typename TSm, typename TA, int Size>
    inline TSm &operator <<(TSm &stream, const VectorN<TA, Size>& value) {
		for (int i=0; i<Size; i++)
			stream << value[i];
		return stream;
	}*/












#define AR_OPERATOR_RightVectorN(op)\
template<typename T, int N, typename NOT_VECTOR(TL), typename R = decltype(std::declval<T>() op std::declval<TL>())>\
    inline vectorN<R,N> operator op (const TL& lhs, const vectorN<T,N>& rhs)  {\
        vectorN<R,N> retval;\
		for (int i = 0; i < N; ++i) retval[i] = lhs op rhs[i];\
        return retval;\
    }

AR_OPERATOR_RightVectorN(+)
AR_OPERATOR_RightVectorN(-)
AR_OPERATOR_RightVectorN(*)
AR_OPERATOR_RightVectorN(/)


#define FUNC_ONE_PARAM(f)\
template<typename T, typename R = decltype(std::f(std::declval<T>())) >\
    inline R f(const T& a) {\
		return std::f(a);\
	}\
template<typename T, int N, typename R = decltype(f(std::declval<T>()))>\
    inline vectorN<R, N> f(const vectorN<T, N>& a) {\
		vectorN<R, N> retval;\
		for (int i = 0; i < N; ++i) {\
			retval[i] = f(a[i]);\
		}\
		return retval;\
	}

/*
#define WRAP_STD_TWO_PARAM(f)\
template<typename TA, typename TB,\
	R = typename std::remove_const<decltype(std::f(std::declval<TA>(),std::declval<TB>()))>::type>\
	R f(const TA a, const TB b) {\
		return std::f(a,b);\
        }*/




#define FUNC_TWO_PARAM(f)\
template<typename TA, typename TB, int N>\
	inline auto f(const vectorN<TA, N>& a, const vectorN<TB, N>& b) -> vectorN<decltype(std::pow(a[0],b[0])),N> {\
		vectorN<decltype(std::pow(a[0],b[0])),N> retval;\
		for (int i = 0; i < N; ++i) {\
			retval[i] = f(a[i],b[i]);\
		}\
		return retval;\
	}\
template<typename NOT_VECTOR(TA), typename TB, int N,\
	typename R = typename std::remove_const<typename std::remove_reference<decltype(f(std::declval<TA>(),std::declval<TB>()))>::type>::type>\
    inline vectorN<R, N> f(const TA a, const vectorN<TB, N>& b)  {\
		vectorN<R, N> retval;\
		for (int i = 0; i < N; ++i) {\
			retval[i] = f(a,b[i]);\
		}\
		return retval;\
	}\
template<typename TA, typename NOT_VECTOR(TB), int N>\
	inline auto f(const vectorN<TA, N>& a, const TB b) -> vectorN<decltype(a[0]*b),N> {\
		vectorN<decltype(std::pow(a[0],b)),N> retval;\
		for (int i = 0; i < N; ++i) {\
			retval[i] = f(a[i],b);\
		}\
		return retval;\
	}


    #ifdef max
    #undef max
    #endif
    #ifdef min
    #undef min
    #endif

template <typename TA, typename TB,	typename R = decltype(std::declval<TA>()*std::declval<TB>())>
R min(const TA& a, const TB& b) {
	return a < b ? a : b;
}
template <typename TA, typename TB,	typename R = decltype(std::declval<TA>()*std::declval<TB>())>
R max(const TA& a, const TB& b) {
	return a > b ? a : b;
}

    FUNC_TWO_PARAM(max)
    FUNC_TWO_PARAM(min)


	template <typename TA, typename TMIN, typename TMAX,
		typename R = decltype(min(max(std::declval<TA>(), std::declval<TMIN>()), std::declval<TMAX>()))>
	//std::remove_const<std::remove_reference<decltype(min(max(std::declval<TA>(), std::declval<TMIN>()), std::declval<TMAX>()))>::type>::type>
		R clamp(const TA& a, const TMIN& _min, const TMAX& _max) {
		return min(max(a, _min), _max);
	}


	template <typename T0, typename T1, typename TE,
		typename R = typename std::remove_const<typename std::remove_reference<decltype((1 - std::declval<TE>())*std::declval<T0>() + std::declval<TE>()*std::declval<T1>())>::type>::type>
		R lerp(const T0 v0, const T1 v1, const TE e) {
		return (1 - e)*v0 + e*v1;
	}

	



	FUNC_ONE_PARAM(abs)
	FUNC_ONE_PARAM(fabs)

	FUNC_ONE_PARAM(acos)
	FUNC_ONE_PARAM(asin)
	FUNC_ONE_PARAM(atan)
	FUNC_TWO_PARAM(atan2)



	FUNC_ONE_PARAM(ceil)
	FUNC_ONE_PARAM(floor)
	//FUNC_ONE_PARAM(ceil)
	//FUNC_ONE_PARAM(frac)

	FUNC_ONE_PARAM(cos)
	FUNC_ONE_PARAM(cosh)

	FUNC_ONE_PARAM(sin)
	FUNC_ONE_PARAM(sinh)


#define RAD_TO_DEG(___rad)\
		(___rad * (180.0 / NettleMath::pi))

#define DEG_TO_RAD(___deg)\
		(___deg * (NettleMath::pi / 180.0))
	
	template<typename T>
	T degrees(const T& a) {
		return RAD_TO_DEG(a);
	}
	template<typename T>
	T radians(const T& a) {
		return DEG_TO_RAD(a);
	}
	
	
	

	FUNC_TWO_PARAM(fmod)


	FUNC_ONE_PARAM(exp)
	//FUNC_TWO_PARAM(pow)
	FUNC_ONE_PARAM(log)
	FUNC_ONE_PARAM(log10)
	FUNC_ONE_PARAM(sqrt)




	template<typename TA, typename NOT_VECTOR(TB), int N>
	inline auto powVec(const vectorN<TA, N>& a, const TB b) -> vectorN<decltype(a[0]*b),N> {
		vectorN<decltype(std::pow(a[0],b)),N> retval;
		for (int i = 0; i < N; ++i) {
			retval[i] = std::pow(a[i],b);
		}
		return retval;
	}

	template<typename TA, typename TB, int N>
	inline auto pow(const vectorN<TA, N>& a, const vectorN<TB, N>& b) -> vectorN<decltype(std::pow(a[0],b[0])),N> {
		return make_array<N>( [&a,&b](size_t i){ return std::pow(a[i],b[i]); } );
	}

	template<typename TA, typename TB, int N>
	inline auto pow(const vectorN<TA, N>& a, const TB& b) -> vectorN<decltype(std::pow(a[0],b)),N> {
		return make_array<N>( [&a,&b](size_t i){ return std::pow(a[i],b); } );
	}

	template<typename TA, typename TB, int N>
	inline auto pow(const TA& a, const vectorN<TA, N>& b) -> vectorN<decltype(std::pow(a,b[0])),N> {
		return make_array<N>( [&a,&b](size_t i){ return std::pow(a,b[i]); } );
	}


	/*
template<typename NOT_VECTOR(TA), typename TB, int N,\
	typename R = typename std::remove_const<typename std::remove_reference<decltype(f(std::declval<TA>(),std::declval<TB>()))>::type>::type>\
	inline vectorN<R, N> f(const TA a, const vectorN<TB, N>& b)  {\
		vectorN<R, N> retval;\
		for (int i = 0; i < N; ++i) {\
			retval[i] = f(a,b[i]);\
		}\
		return retval;\
	}\
template<typename TA, typename NOT_VECTOR(TB), int N>\
	inline auto f(const vectorN<TA, N>& a, const TB b) -> vectorN<decltype(a[0]*b),N> {\
		vectorN<decltype(std::pow(a[0],b)),N> retval;\
		for (int i = 0; i < N; ++i) {\
			retval[i] = f(a[i],b);\
		}\
		return retval;\
	}*/




	template<typename T>
	inline vectorN<T, 3> cross(const vectorN<T, 3>& a, const vectorN<T, 3>& b) {
		T x = a.y * b.z - a.z * b.y ;
		T y = a.z * b.x - a.x * b.z ;
		T z = a.x * b.y - a.y * b.x ;
		return vectorN<T, 3>(x,y,z);
	}


	template<typename TA, typename TB, int N, typename Func>
	inline auto funcTwoParam(const vectorN<TA, N>& a, const vectorN<TB, N>& b, Func func) -> vectorN<typename std::result_of<Func(TA, TB)>::type, N>
	{
		vectorN<typename std::result_of<Func(TA, TB)>::type, N> retval;
		for (int i = 0; i < N; ++i)
			retval[i] = func(a[i], b[i]);
		return retval;
	}


	template<typename TA, typename TB, int N, typename TR = MUL_TYPE(TA, TB)>
    inline TR dot(const vectorN<TA, N>& a, const vectorN<TB, N>& b) {
		TR retval = a[0] * b[0];
		for (int i = 1; i < N; ++i)
			retval += a[i] * b[i];
		return retval;
	}

	template<typename T, int N>
    inline T length(const vectorN<T, N>& a) {
		return a.length();
	}
	
	template<typename T, int N>
        inline vectorN<T, N> normalize(const vectorN<T, N>& a) {
		return a / a.length();
	}

        template<typename TA, typename TB, int N, typename TR = MUL_TYPE(TA,TB)>
        inline vectorN<TR, N> flatten(const vectorN<TA, N>& a, const vectorN<TB, N>& normal) {
                return a - normal * dot(a,normal);
        }
	
	

//int1x4    iMatrix;   // integer matrix with 1 row, 4 columns
//int4x1    iMatrix;   // integer matrix with 4 rows, 1 column
#define MATRIX_N(T,N)\
	typedef vectorN<vectorN<T, N>, 2> T##2x##N;\
	typedef vectorN<vectorN<T, N>, 3> T##3x##N;\
	typedef vectorN<vectorN<T, N>, 4> T##4x##N;

#define MATRICES(T)\
	MATRIX_N(T,2)\
	MATRIX_N(T,3)\
	MATRIX_N(T,4)\

	MATRICES(float)
	MATRICES(double)
	MATRICES(int)
	MATRICES(bool)
    MATRICES(short)

#define VECTORS(T)\
	typedef vectorN<T, 2> T##2;\
	typedef vectorN<T, 3> T##3;\
	typedef vectorN<T, 4> T##4;


	VECTORS(float)
	VECTORS(double)
	VECTORS(int)
	VECTORS(bool)
	VECTORS(short)

	typedef int64_t longlong;
	VECTORS(longlong)

	template<typename A, typename B, typename R = decltype(std::declval<A>() + std::declval<B>())>
	vectorN<R, 2> common2(A a, B b) {
		return vectorN<R, 2>(a, b);
	}

	template<typename A, typename B, typename C, typename R = decltype(std::declval<A>() + std::declval<B>() + std::declval<C>())>
	vectorN<R, 3> common3(A a, B b, C c) {
		return vectorN<R, 3>(a, b, c);
	}



}


#endif // NVectorBase_h__
