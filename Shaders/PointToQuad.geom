#version 400 core
#pragma include VertexFormats.inc.vert

layout( lines ) in; // layout( points ) in;
layout( triangle_strip, max_vertices = 4 ) out;



in ParticleVertex ourColorTes[];


//in EyeSpaceVertex {
//	vec3 position;
//} gs_in[];





out WireframeVertex {
	vec2 texCoord;
	vec4 color;
} gs_out;




uniform mat4 mvp;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 cameraPosition;
uniform float time;
uniform float particleSize;

//vec2 transformToViewport( const in vec4 p ) {
//	return vec2( viewportMatrix * ( p / p.w ) );
//}


float rand(float n){
	return fract(sin(n) * 43758.5453123);
}


float rand(vec2 n) {
	return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}


float noise(float p){
	float fl = floor(p);
	float fc = fract(p);
	return mix(rand(fl), rand(fl + 1.0), fc);
}


float noise(vec2 n) {
	const vec2 d = vec2(0.0, 1.0);
	vec2 b = floor(n);
	vec2 fractN = fract(n);
	vec2 f = smoothstep(vec2(0.0), vec2(1.0), fractN);

	return mix(
		mix(rand(b), rand(b + d.yx), f.x),
		mix(rand(b + d.xy), rand(b + d.yy), f.x)
		, f.y);
}






void main()
{
	//float xSize = projectionMatrix[0][0] * particleSize;
	//float ySize = projectionMatrix[1][1] * particleSize;
	float xSize = projectionMatrix[0][0] * (ourColorTes[0].size + particleSize);
	float ySize = projectionMatrix[1][1] * (ourColorTes[0].size + particleSize);

	vec4 position = mvp * gl_in[0].gl_Position;
	vec4 pos  = position + vec4(-xSize/2, -ySize/2, 0, 0);
	vec4 pos2 = position + vec4( xSize/2,  ySize/2, 0, 0);


	//vec4 ourColor = vec4(0,0,1,1);
	vec4 ourColor = ourColorTes[0].color;
	//vec4 ourColor = ourColorVs[0];


	gs_out.texCoord = vec2(0,0);
	gs_out.color = ourColor;
	gl_Position = pos;
	EmitVertex();

	gs_out.texCoord = vec2(1,0);
	gs_out.color = ourColor;
	gl_Position = vec4(pos2.x, pos.yzw);
	EmitVertex();

	gs_out.texCoord = vec2(0,1);
	gs_out.color = ourColor;
	gl_Position = vec4(pos.x, pos2.y, pos.zw);
	EmitVertex();

	gs_out.texCoord = vec2(1,1);
	gs_out.color = ourColor;
	gl_Position = vec4(pos2.x, pos2.y, pos.zw);
	EmitVertex();



	EndPrimitive();
}
