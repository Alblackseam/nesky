#pragma once

#include "Place.h"
#include <QObject>
#include <QVector3D>
#include <QMatrix4x4>


struct QPlace : public QObject, public Place<QVector3D>
{
	Q_OBJECT
	using Base = Place<QVector3D>;

	Q_PROPERTY(QVector3D up MEMBER u WRITE setUp NOTIFY upChanged())
	Q_PROPERTY(QVector3D right MEMBER r WRITE setRight NOTIFY rightChanged())
	Q_PROPERTY(QVector3D front MEMBER f WRITE setFront NOTIFY frontChanged())
	Q_PROPERTY(QVector3D pos MEMBER p WRITE setPos NOTIFY posChanged())
	Q_PROPERTY(QMatrix4x4 world READ worldMatrix() NOTIFY worldChanged())

public:
	QMatrix4x4 worldMatrix() const {
		return Base::worldMatrix<QMatrix4x4>();
	}

public slots:
	void turnUp(float phi) {
		Base::turnUp(phi);
	}

	void turnDown(float phi) {
		Base::turnDown(phi);
	}

	void turnLeft(float phi) {
		Base::turnLeft(phi);
	}

	void turnRight(float phi) {
		if (std::abs(phi) < std::numeric_limits<float>::denorm_min())
			return;
		Base::turnRight(phi);
		emit rightChanged();
		emit frontChanged();
	}


	void setScale(float scale_) {
		Base::scale = scale_;
	}


	void rotateAroundY(float phi) {
		Base::rotateAroundY(phi);
	}


	void roll(float phi) {
		Base::roll(phi);
	}

	void onward(float len){
		Base::onward(len);
	}

	void backward(float len){ Base::backward(len); }
	void strafeLeft(float len){ Base::strafeLeft(len); }
	void strafeRight(float len){ Base::strafeRight(len); }
	void strafeUp(float len){ Base::strafeUp(len); }
	void strafeDown(float len){ Base::strafeDown(len); }

	void setRight(const QVector3D& v) { Base::r = v; rightChanged(); }
	void setUp(const QVector3D& v) { Base::u = v; upChanged(); }
	void setFront(const QVector3D& v) { Base::f = v; frontChanged(); }
	void setPos(const QVector3D& v) {
		Base::setPos(v);
		posChanged();
		worldChanged();
	}

signals:
	void upChanged();
	void rightChanged();
	void frontChanged();
	void posChanged();
	void worldChanged();

};
