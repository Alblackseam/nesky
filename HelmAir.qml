import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Logic 2.0
import test 1.0

Entity {
	id: root
	property var place // QPlace
	property var transform
	property var shoter
	property bool accelerate

	property bool cameraByPlace: false
	property vector2d speed
	property real rotationRight: 0
	property real accelerationValue: 0.001


	function update(dt, keys){
		rotationRight = 0;
		if (keys[Qt.Key_Right] === true) rotationRight = 4
		if (keys[Qt.Key_Left] === true) rotationRight = -4

		if (keys[Qt.Key_Shift] === true){
			//var acceleration = Qt.vector2d(place.front.x * accelerationValue, place.front.y * accelerationValue);
			speed.x += place.front.x * accelerationValue;
			speed.y += place.front.y * accelerationValue;
			//console.log(speed)

			if (accelerate !== true) accelerate = true
		} else{
			if (accelerate !== false) accelerate = false
		}

		//if (keys[Qt.Key_Control] === true) speed -= 1
		if (keys[Qt.Key_Space] === true)
			if (shoter !== undefined)
				shoter.shot(place);


		place.turnRight(dt * rotationRight)


		//console.log(speed)
		//console.log(Qt.vector3d(place.pos.x + speed.x, place.pos.y + speed.y, 0))

		place.setPos(Qt.vector3d(place.pos.x + speed.x, place.pos.y + speed.y, 0))
		//place.onward(dt * speed / 1000.0)


		transform.matrix = place.world
	}

	function updateCamera(dt, camera){
		if (cameraByPlace === true){
			//var cameraPos = place.pos.plus(place.up.times(20))
			camera.position = Qt.vector3d(place.pos.x, place.pos.y, camera.position.z)
			camera.upVector = place.up
			camera.viewCenter = place.pos
		}
	}
}
