#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>
#include <QOpenGLContext>
#include "QPlace.h"
#include "Shoter.h"
#include "FireParticles.h"
#include "Stars.h"


int main(int argc, char **argv)
{

	QGuiApplication app(argc, argv);
	//QOpenGLContext::openGLModuleType();

	QSurfaceFormat format;
	if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL) {
		format.setVersion(4, 0);
		format.setProfile(QSurfaceFormat::CoreProfile);
	}
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
	//format.setSamples(4);
	format.setSwapInterval(1);
	QSurfaceFormat::setDefaultFormat(format);

	qmlRegisterType<Shoter>("test", 1, 0, "Shoter");
	qmlRegisterType<QPlace>("test", 1, 0, "QPlace");
	qmlRegisterType<BulletsGeometry>("test", 1, 0, "BulletsGeometry");
	qmlRegisterType<FireParticles>("test", 1, 0, "FireParticles");
	qmlRegisterType<Stars>("test", 1, 0, "Stars");

    QQuickView view;
    view.setFormat(format);
    view.setResizeMode(QQuickView::SizeRootObjectToView);
	view.setSource(QUrl("qrc:/Main.qml"));
    view.setColor("#000000");
    view.show();

    return app.exec();
}
