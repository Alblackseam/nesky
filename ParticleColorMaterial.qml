
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import QtQuick 2.1

Material {
    id: root

	property alias diffuse: diffuseTextureImage.source
	property vector3d cameraPosition: Qt.vector3d(0, 0, 0)
	property real time: 0.0
	property real particleSize: 1.0
	property vector4d particleColor: Qt.vector4d(1, 1, 1, 1)

	parameters: [
		Parameter { name: "cameraPosition"; value: root.cameraPosition },
		Parameter { name: "time"; value: root.time },
		Parameter { name: "color"; value: root.particleColor },
		Parameter { name: "particleSize"; value: root.particleSize },
		Parameter {
			name: "diffuseTexture"
			value: Texture2D {
				id: diffuseTexture
				minificationFilter: Texture.LinearMipMapLinear
				magnificationFilter: Texture.Linear
				wrapMode {
					x: WrapMode.Repeat
					y: WrapMode.Repeat
				}
				generateMipMaps: true
				maximumAnisotropy: 16.0
				TextureImage { id: diffuseTextureImage; }
			}
		}
	]


    effect: Effect {
        RenderPass {
            id: glpass
			//filterKeys: [ FilterKey { name: "pass"; value: "forward" } ]
			//filterKeys: [ FilterKey { name: "pass"; value: "blendpass" } ]

			renderStates: [
				//DepthTest { depthFunction: DepthTest.Always },
				NoDepthMask{
				},
				BlendEquation{
					blendFunction: BlendEquation.Add
				},
				BlendEquationArguments {
					//sourceRgb: BlendEquationArguments.ConstantColor // SourceAlphaSaturate
					//destinationRgb: BlendEquationArguments.ConstantColor // OneMinusSourceAlpha
					sourceRgb: BlendEquationArguments.One
					destinationRgb: BlendEquationArguments.One
					sourceAlpha: BlendEquationArguments.SourceAlpha
					destinationAlpha: BlendEquationArguments.One
				}
			]

			shaderProgram: ShaderProgram {
				vertexShaderCode: loadSource("qrc:/Shaders/passthru.vert")
				tessellationControlShaderCode: loadSource("qrc:/Shaders/quads.tcs")
				tessellationEvaluationShaderCode: loadSource("qrc:/Shaders/quads.tes")
				geometryShaderCode: loadSource("qrc:/Shaders/PointToQuad.geom")
				fragmentShaderCode: loadSource("qrc:/Shaders/ColoredTexturedParticle.frag")
			}
		}

        techniques: [
            Technique {
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
					majorVersion: 3
					minorVersion: 2
                }

                filterKeys: [ FilterKey { name: "renderingStyle"; value: "forward" } ]

                renderPasses: [ glpass ]
            }
        ]
    }
}

