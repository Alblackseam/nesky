import QtQuick 2.0 as QQ2
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Logic 2.0
import Qt3D.Extras 2.0
import test 1.0


Entity {
	SphereMesh {
		id: sphereMesh
		radius: 1
	}

	Transform {
		id: starTransform
		translation: Qt.vector3d(5, 5, 0) //scale3D: Qt.vector3d(1000, 1000, 1000)
	}

	Transform {
		id: redStarTransform
		translation: Qt.vector3d(7, 5, 0)
	}

	Transform {
		id: greenStarTransform
		translation: Qt.vector3d(5, 7, 0)
	}

	Transform {
		id: blueStarTransform
		translation: Qt.vector3d(5, 5, 3)
	}

	PhongMaterial {
		id: planetMaterial
		ambient: Qt.rgba(0.2, 0.1, 0.1, 1)
		diffuse: Qt.rgba(0.8, 0.8, 0.1, 1)
		shininess: 1
		specular: Qt.rgba(0.1, 0.1, 0.1, 1)
	}

	PhongMaterial {
		id: redMaterial
		ambient: Qt.rgba(0.2, 0.1, 0.1, 1)
		diffuse: Qt.rgba(0.8, 0.1, 0.1, 1)
		shininess: 0.1
		specular: Qt.rgba(0.1, 0.1, 0.1, 1)
	}

	PhongMaterial {
		id: greenMaterial
		ambient: Qt.rgba(0.0, 0.2, 0.0, 1)
		diffuse: Qt.rgba(0.0, 0.8, 0.0, 1)
		shininess: 0.1
		specular: Qt.rgba(0.1, 0.1, 0.1, 1)
	}

	PhongMaterial {
		id: blueMaterial
		ambient: Qt.rgba(0.0, 0.0, 0.2, 1)
		diffuse: Qt.rgba(0.0, 0.0, 0.8, 1)
		shininess: 0.1
		specular: Qt.rgba(0.1, 0.1, 0.1, 1)
	}


	ParticleColorMaterial {
		id: particleMaterial
		diffuse: "qrc:/Resources/Star32.png"
		particleSize: 30
		particleColor: Qt.vector4d(1, 1, 0, 1)
	}

	// Sun
	Entity {
		components: [
			DirectionalLight {
				color: Qt.rgba(1, 1, 1, 1.0)
				worldDirection: Qt.vector3d(1, 1, -5)
				intensity: 0.6
			}
		]
	}

	// Sun
	Entity {
		components: [
			DirectionalLight {
				color: Qt.rgba(0.5, 0.5, 1, 1.0)
				worldDirection: Qt.vector3d(1, 1, -5)
				intensity: 1.0
			}
		]
	}

	// Blue Sun
	Entity {
		components: [
			DirectionalLight {
				color: Qt.rgba(0.5, 0.5, 1, 1.0)
				worldDirection: Qt.vector3d(-1, 1, 3)
				intensity: 0.9
			}
		]
	}

	// Planets
	Entity { components: [sphereMesh, planetMaterial, starTransform] }
	Entity { components: [sphereMesh, redMaterial, redStarTransform] }
	Entity { components: [sphereMesh, greenMaterial, greenStarTransform] }
	Entity { components: [sphereMesh, blueMaterial, blueStarTransform] }


	/*
	Entity {
		GeometryRenderer{
			id: starsRenderer
			instanceCount: 1
			indexOffset: 0
			firstInstance: 0
			primitiveType: GeometryRenderer.Patches
			verticesPerPatch: 1
			geometry: Geometry{
				Attribute{
					name: "vertexPosition"
					attributeType: Attribute.VertexAttribute
					vertexBaseType: Attribute.Float
					vertexSize: 3
					byteOffset: 0
					divisor: 0
					byteStride: sizeOfParticleData * floatSize
					count: particlesCount

					buffer: Buffer{
						type: Buffer.VertexBuffer
						data: buildParticlesBuffer()
					}
				}
			}
		} // GeometryRenderer

		Transform {
			id: starsTransform
		}

		components: [
			starsRenderer,
			particleMaterial,
			starsTransform
		]
	} // Stars
	*/

	Entity {
		components: [
			GeometryRenderer{
				instanceCount: 1
				indexOffset: 0
				firstInstance: 0
				primitiveType: GeometryRenderer.Patches
				verticesPerPatch: 1
				geometry: Stars {
					id: stars
				}
			}, // GeometryRenderer
			ParticleColorMaterial {
				diffuse: "qrc:/Resources/Star32.png"
				particleSize: 100
			}
		]
	} // New Stars



	QPlace{
		id: earthPlace
	}

	Entity {
		components: [
			SphereMesh{
				generateTangents: true
				rings: 64
				slices: 64
			},
			NormalDiffuseSpecularMapMaterial {
				ambient: Qt.rgba(0.4, 0.4, 0.4, 1)
				diffuse: TextureLoader { source: "qrc:/Resources/earthmap2k.jpg" }
				normal: TextureLoader { source: "qrc:/Resources/earthnormal2k.jpg" }
				specular: TextureLoader { source: "qrc:/Resources/earthspec2k.jpg" }
				shininess: 150
			},
			Transform {
				matrix: earthPlace.world
			}
		]
	} // Earth

	FrameAction {
		onTriggered: {
			var newTime = particleMaterial.time + dt
			while (newTime >= 1)
				newTime -= 1
			particleMaterial.time = newTime

			currentTime += dt
			stars.update(dt)

			earthPlace.pos = Qt.vector3d(10, 150, 0)
			earthPlace.setScale(100)
			earthPlace.rotateAroundY(0.00002)
		}
	}

	property real currentTime: 0
	property int particlesCount: 100000
	readonly property int floatSize: 4
	readonly property int sizeOfParticleData: 3;
	function buildParticlesBuffer() {
		var factor = 10000.0;
		var bufferData = new Float32Array(particlesCount * sizeOfParticleData);
		for (var i = 0; i < particlesCount; ++i) {
			var positionIdx = i * sizeOfParticleData;
			for (var j = 0; j < 2; ++j)
				bufferData[positionIdx + j] = (Math.random() - 0.5) * factor;
			bufferData[positionIdx + 2] = -100 - Math.random() * 1000;
		}
		return bufferData
	}
}
